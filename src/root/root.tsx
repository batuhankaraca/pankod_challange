import {Provider} from 'react-redux';
import {Store} from 'redux';

import App from '../App';
import IAppState from '../store/IAppState.interface';
import React from 'react';


interface IPros {
    store: Store<IAppState>;
}


const Root: React.SFC<IPros> = props =>{
    return(
        <Provider store={props.store}>
            <App/>
        </Provider>
    );
};

export default Root;