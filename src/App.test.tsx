import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import renderer from 'react-test-renderer';

/*test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});*/

test("snapshot test", () =>{
  const tree = renderer.create(<App/>).toJSON();
  console.log(tree);
  expect(tree).toMatchSnapshot();
  
})
