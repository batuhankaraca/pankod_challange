import {
    applyMiddleware,
    combineReducers,
    createStore,
    Store
  } from 'redux';

  import createSagaMiddleware from 'redux-saga';
  import IAppState from './IAppState.interface';