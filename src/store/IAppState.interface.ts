import ICharacterState from "../titles/data/titlestate.interface";

export default interface IAppState {
  characterState: ICharacterState;
}