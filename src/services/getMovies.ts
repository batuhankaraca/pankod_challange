import { useEffect, useState } from 'react';
import { Service } from '../types/Service';
import { Title } from '../api/Title';

export interface Titles {
  results: Title[];
}

const useMoviesService = () => {
  const [result, setResult] = useState<Service<Titles>>({
    status: 'loading'
  });

  useEffect(() => {
    fetch('https://raw.githubusercontent.com/pankod/frontend-challenge/master/feed/sample.json')
      .then(response => response.json())
      .then(response => setResult({ status: 'loaded', payload: response }))
      .catch(error => setResult({ status: 'error', error }));
      console.log("hello");
      
  }, []);

  return result;
};

export default useMoviesService;