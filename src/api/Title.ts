export interface Title {
    title: string;
    releaseYear: string;
    programType: string;
    images: any;
  }