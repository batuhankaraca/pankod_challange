import axios from 'axios';

const baseUrl = 'https://raw.githubusercontent.com/pankod/frontend-challenge/master/feed/sample.json';

export const getCharactersFromApi = (): Promise<any> => {
  return axios.get(`${baseUrl}`);
}

export const searchCharactersFromApi = (term: String): Promise<any> => {
  return axios.get(`${baseUrl}/people/?search=${term}`);
}