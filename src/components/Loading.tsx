import React from 'react';

const Loading: React.FC<{}> = () => <div className="section_container loader-container"><div className="loader" /></div>;

export default Loading;