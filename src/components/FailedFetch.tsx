import React from 'react';

const FailedFetch: React.FC<{}> = () => <div className="container section_container"><h2>Yüklenirken Bir Hata Oluştu...</h2></div>;

export default FailedFetch;