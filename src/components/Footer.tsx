/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { Link } from 'react-router-dom';

function Footer() {
    return (
        <div className="footer">
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/">Terms and Condition</Link></li>
                <li><Link to="/">Privacy Policy</Link></li>
                <li><Link to="/">Collection Statement</Link></li>
                <li><Link to="/">Help</Link></li>
                <li className="last_link"><Link to="/">Manage Account</Link></li>
            </ul>
            <ul>
                <p>Copyright 2016 DEMO Streaming. All Rights Reserved.</p>
            </ul>
            <ul>
                <img src={require('../images/facebook.svg')} className="social" />
                <img src={require('../images/instagram.svg')} className="social" />
                <img src={require('../images/twitter.svg')} className="social" />
                <img src={require('../images/appstore.svg')} className="download" />
                <img src={require('../images/googleplay.svg')} className="download" />


            </ul>
        </div>
    );
}

export default Footer;