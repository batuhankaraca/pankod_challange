import React from 'react';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Movies from './pages/Movies';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Series from './pages/Series';
import useMoviesService from './services/getMovies';

class App extends React.Component {
 
  render(){
    return (
      <Router>
        <div className="App">
          <Navbar/>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/Movies" component={Movies} />
            <Route path="/Series" component={Series} />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
  
}

export default App;
