import Ititle from './title.interface';

export default interface titlestate {
  readonly character?: Ititle,
  readonly characters: Ititle[],
  readonly isFetching: Boolean,
}