import React from 'react';
import { Link } from 'react-router-dom';

export class Home extends React.Component {
    render() {
        return (
            <div className="App">
                <div className="row pagename">
                        <div className="col-sm-10 offset-xl-1">
                            <h3>Popular Titles</h3>
                        </div>
                    </div>
                <div className="container section_container">
                    <div className="row">
                        <div className="col-xl-3 col-md-6">
                            <div className="movies">
                                <Link to="/Movies">
                                    <h1>Movies</h1>
                                </Link>
                            </div>
                        </div>
                        <div className="col-xl-3 col-md-6">
                            <div className="movies">
                                <Link to="/Series">
                                    <h1>Series</h1>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
