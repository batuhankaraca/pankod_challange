/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import Select from 'react-select';
import Loading from '../components/Loading';
import FailedFetch from '../components/FailedFetch';
import useMoviesService from '../services/getMovies';
import {getCharactersFromApi} from '../api/api';

const options = [
    { value: 'descyear', label: 'En Eski Tarihe Göre Sırala' },
    { value: 'ascyear', label: 'En Yeni Tarihe Göre Sırala' },
    { value: 'desctitle', label: 'İsme Göre Ters Sırala' },
    { value: 'asctitle', label: 'İsme Göre Sırala' },
  ];

  const Movies: React.FC<{}> = () => {
    const service = useMoviesService();
    const [url] = React.useState('');
    console.log(service);

    const a = getCharactersFromApi();
    console.log(a);
    
    
  
    return (
      <div className="container section_container">
        <div className="card">
          {service.status === 'loading' && (
            <div className="loader-container">
              <Loading />
            </div>
          )}
           <div className="row">
          {service.status === 'loaded' &&
            service.payload.entries.map((movies: { images: { [x: string]: { url: string | undefined; }; }; title: React.ReactNode; releaseYear: React.ReactNode; }) => (
                <div className="col-sm-3 movies_list">
                <img src={movies.images['Poster Art'].url} />
                <div className="title_div">
                    <p>{movies.title} <br/> {movies.releaseYear}</p>
                </div>
            </div>
            
            ))}</div>
          {!!url}
        </div>
        
      </div>
    );
  };


export default Movies;
