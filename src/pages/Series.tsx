/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
export class Series extends React.Component<any, any>{

    constructor(props: any) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: [],
        };
    }

    async componentDidMount() {
        fetch("https://raw.githubusercontent.com/pankod/frontend-challenge/master/feed/sample.json")
            .then(response => response.json())
            .then((result) => {
                result.entries = result.entries.filter((res: { programType: string; }) => {
                    return res.programType.match('series');
                });
                result.entries = result.entries.filter((res: any) => {
                    return res.releaseYear > 2010;
                });
                result.entries = result.entries.sort(function (a: any, b: any) {
                    if (a.title < b.title) { return -1; }
                    if (a.title > b.title) { return 1; }
                    return 0;
                })
                result.entries = result.entries.slice(0, 21);               

                this.setState({
                    isLoaded: true,
                    posts: result.entries,
                    alldata: result.entries
                });
            },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    SearchInput(e: any) {
        if (e.target.value.length > 2) {
            var search = e.target.value;
            this.setState({
                posts: this.state.posts.filter((item: { title: { indexOf: (arg0: any) => number; }; }) => item.title.indexOf(search) !== -1)
            })
        }
        else {
            this.setState({
                posts: this.state.alldata
            })
        }
    }

    SelectInput(event: any) {

        switch (event.target.value) {
            case 'descyear':
                this.setState({
                    posts: this.state.posts.sort(function (a: any, b: any) {
                        if (a.releaseYear < b.releaseYear) { return -1; }
                        if (a.releaseYear > b.releaseYear) { return 1; }
                        return 0;
                    })
                })
                break;
            case 'ascyear':
                this.setState({
                    posts: this.state.posts.sort(function (a: any, b: any) {
                        if (b.releaseYear < a.releaseYear) { return -1; }
                        if (b.releaseYear > a.releaseYear) { return 1; }
                        return 0;
                    })
                })
                break;
            case 'desctitle':
                this.setState({
                    posts: this.state.posts.sort(function (a: any, b: any) {
                        if (b.title < a.title) { return -1; }
                        if (b.title > a.title) { return 1; }
                        return 0;
                    })
                })
                break;
            case 'asctitle':
                this.setState({
                    posts: this.state.posts.sort(function (a: any, b: any) {
                        if (a.title < b.title) { return -1; }
                        if (a.title > b.title) { return 1; }
                        return 0;
                    })
                })
                break;
        }
    }

    render() {
        const { error, isLoaded, posts } = this.state;
        if (error) {
            return <div className="container section_container"><h2>Yüklenirken Bir Hata Oluştu...</h2></div>
        } else if (!isLoaded) {
            return <div className="container section_container"><h2>Yükleniyor...</h2></div>
        } else {
            return (

                <div className="container section_container">
                    <div className="row pagename">
                        <div className="col-sm-10 offset-xl-1">
                            <h3>Popular Series</h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 col-xl-4">
                            <input type="text" placeholder="Search" onChange={(e: any) => this.SearchInput(e)} />
                        </div>
                        <div className="col-6 col-xl-4 offset-xl-4">
                            <select id="sortitems" onChange={(e: any) => this.SelectInput(e)}>
                                <option value="empty">Sıralama Türünü Seçiniz</option>
                                <option value="descyear">En Eski Tarihe Göre Sırala</option>
                                <option value="ascyear">En Yeni Tarihe Göre Sırala</option>
                                <option value="desctitle">İsme Göre Ters Sırala</option>
                                <option value="asctitle">İsme Göre Sırala</option>
                            </select>
                            <p>{this.state.value}</p>
                        </div>

                    </div>
                    <div className="row">
                        {
                            posts.map((post: { images: { [x: string]: { url: string | undefined; }; }; title: React.ReactNode; releaseYear: React.ReactNode; }) => (
                                <div className="col-sm-3 movies_list">
                                    <img src={post.images['Poster Art'].url} />
                                    <div className="title_div">
                                        <p>{post.title} <br/> {post.releaseYear}</p>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            );
        }
    }
}

export default Series;
